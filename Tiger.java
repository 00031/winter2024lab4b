public class Tiger {
	 private int size;
	 private String type;
	 private int damage;
	 
	 public void introduce() {
  		 System.out.println("grrr im a " + type + "tiger and i'm " + size + " cm");
	 }
	 public void attack() {
		 System.out.println("Hahaha I've dealt " + damage + " damage on you!");
	 }
	 //set method
	 public void setDamage (int damage) {
     this.damage = damage;
	 }
	 
	 //get method
	 public int getSize () {
		 return this.size; 
	 }
	 public String getType () {
		 return this.type; 
	 }
	 public int getDamage () {
		 return this.damage; 
	 }
	 
	 // constructor 
	 public Tiger(int size, int damage, String type){
		 this.size = size;
		 this.type = type;
		 this.damage = damage;
	 }
		 
	 
}
	 